const expect = require('expect');
const utils = require("./utils");

describe('Utils', () => {

  describe("#add", () => {
    it('should add two numbers', () => {
      var res = utils.add(33, 11);
      expect(res).toBe(44).toBeA('number');
    });
    it('should add two numbers async', (done) => {
      utils.asyncAdd(33, 11, (sum) => {
        expect(sum).toBe(44).toBeA('number');
        done();
      });
    });
  })

  describe("#square", () => {
    it('should square a number', (done) => {
      utils.asyncSquare(3, (total) => {
        expect(total).toBe(9).toBeA('number');
        done();
      });
    });

    it('should square a number', () => {
      var res = utils.square(13);
      expect(res).toBe(169).toBeA('number');
    });

  });

  it('should set the name', () => {
    var user = { age: 29, location: "Boston" };

    var res = utils.setName(user, "Tyrel Souza");

    expect(res).toInclude({
      age: user.age,
      firstName: "Tyrel",
      lastName: "Souza",
    }).toBeA('object');
  })

})



// it('shouled expect some values', () => {
//   // expect(12).toNotBe(11)
//   // expect({name: 'Tyrel'}).toEqual({name: 'Tyrel'});
//   // expect([2, 3, 4]).toInclude(4)
//   // expect({
//   //     name: 'Tyrel',
//   //     age: 29,
//   //     location: 'Boston'
//   // }).toInclude({age: 29})
//
// })
