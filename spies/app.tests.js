const expect = require('expect');
const reqire = require('rewire');

var app = reqire('./app');
// app.__set__
// app.__get__
describe('App', () => {
  var db = {
    saveUser: expect.createSpy()
  }
  app.__set__('db', db);

  it('should call the spy correctly', () => {
    var spy = expect.createSpy();
    spy('Andrew', 25);
    expect(spy).toHaveBeenCalledWith('Andrew', 25);
  });

  it('should call saveUser with user object', () => {
    var email = 'tits@tits.com'
    var password = 'fakePass'
    app.handleSignup(email, password);
    expect(db.saveUser).toHaveBeenCalledWith({email, password})
  })
})
